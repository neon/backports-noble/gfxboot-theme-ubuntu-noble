# Afrikaans translation for gfxboot-theme-ubuntu
# Copyright (c) (c) 2006 Canonical Ltd, and Rosetta Contributors 2006
# This file is distributed under the same license as the gfxboot-theme-ubuntu package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: gfxboot-theme-ubuntu\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2016-04-16 01:46+0000\n"
"PO-Revision-Date: 2020-04-07 11:35+0000\n"
"Last-Translator: bernard stafford <Unknown>\n"
"Language-Team: Afrikaans <af@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2020-04-10 15:08+0000\n"
"X-Generator: Launchpad (build 2e26c9bbd21cdca248baaea29aeffb920afcc32a)\n"

#. ok button label
#. txt_ok
msgid "OK"
msgstr "OK"

#. cancel button label
#. txt_cancel
msgid "Cancel"
msgstr "Kanselleer"

#. reboot button label
#. txt_reboot
msgid "Reboot"
msgstr "Herselflaai"

#. continue button label
#. txt_continue
msgid "Continue"
msgstr "Voortgaan"

#. txt_bootoptions
msgid "Boot Options"
msgstr "Stewel Opsies"

#. window title for exit dialog
#. txt_exit_title (see txt_exit_dialog)
msgid "Exiting..."
msgstr "Uitgang..."

#. txt_exit_dialog
msgid ""
"You are leaving the graphical boot menu and\n"
"starting the text mode interface."
msgstr ""
"U verlaat nou die grafiese selflaaikieslys en\n"
" begin die teksmoduskoppelvlak."

#. txt_help
msgid "Help"
msgstr "Hulp"

#. info box title
#. txt_info_title
msgid "Boot loader"
msgstr "Stewel laaier"

#. error box title
#. txt_error_title
msgid "I/O error"
msgstr "I/O fout"

#. boot disk change dialog title
#. txt_change_disk_title
msgid "Change Boot Disk"
msgstr "Verandering Selflaai Skyf"

#. txt_insert_disk
#, c-format
msgid "Insert boot disk %u."
msgstr "Insetsel stewel skyf %u."

#. txt_insert_disk2
#, c-format
msgid ""
"This is boot disk %u.\n"
"Insert boot disk %u."
msgstr ""
"Hierdie is stewel skyf %u. \n"
"Insetsel  stewel skyf %u."

#. txt_insert_disk3
#, c-format
msgid ""
"This is not a suitable boot disk.\n"
"Please insert boot disk %u."
msgstr ""
"Hierdie is nie 'n geskik stewel skyf. \n"
"Asseblief voeg stewel skyf %u."

#. password dialog title
#. txt_password_title
msgid "Password"
msgstr "Wagwoord"

#. Keep the newlines and spaces after ':'!
#. txt_password
msgid ""
"Enter your password:   \n"
"\n"
"\n"
msgstr ""
"Voer u wagwoord in:   \n"
"\n"
"\n"

#. dvd warning title
#. txt_dvd_warning_title
msgid "DVD Error"
msgstr "DVD Fout"

#. txt_dvd_warning2
msgid ""
"This is a two-sided DVD. You have booted from the second side.\n"
"\n"
"Turn the DVD over then continue."
msgstr ""
"Hierdie is 'n tweesydige DVD. Jy het van die tweede-kant af gelaai\n"
"\n"
"Draai die DVD om en gaan dan voort."

#. power off dialog title
#. txt_power_off_title
msgid "Power Off"
msgstr "Krag af"

#. txt_power_off
msgid "Halt the system now?"
msgstr "Krag af stelsel nou?"

#. txt_password
msgid "Password\n"
msgstr "Wagwoord\n"

#. button label for other/more options
#. txt_other_options
msgid "Other Options"
msgstr "Ander Opsies"

#. label for language selection
#. txt_language
msgid "Language"
msgstr "Taal"

#. label for keymap selection
#. txt_keymap
msgid "Keymap"
msgstr "Sleutelkaart"

#. label for installation mode selection
#. txt_modes
msgid "Modes"
msgstr "Modusse"

#. label for modes menu
#. txt_mode_normal
msgid "Normal"
msgstr "Normaal"

#. label for d-i mode menu
#. txt_expert_mode
msgid "Expert mode"
msgstr "Kundige modus"

#. title for accessibility menu
#. txt_access
msgid "Accessibility"
msgstr "Toeganklikheid"

#. label for accessibility menu
#. txt_access_none
msgid "None"
msgstr "Geeneen"

#. label for accessibility menu
#. txt_access_v1
msgid "High Contrast"
msgstr "Hoë Kontras"

#. label for accessibility menu
#. txt_access_v2
msgid "Magnifier"
msgstr "Vergrooter"

#. label for accessibility menu
#. txt_access_v3
msgid "Screen Reader"
msgstr "Skerm Leser"

#. label for accessibility menu
#. txt_access_brltty
msgid "Braille Terminal"
msgstr "Braille-Terminaal"

#. label for accessibility menu
#. txt_access_m1
msgid "Keyboard Modifiers"
msgstr "Sleutelbord Wysigers"

#. label for accessibility menu
#. txt_access_m2
msgid "On-Screen Keyboard"
msgstr "Op-Skerm Sleutelbord"

#. label for accessibility menu
#. txt_access_m3
msgid "Motor Difficulties - switch devices"
msgstr "Motore moeilik probleem - skakelaar toestelle"

#. label for accessibility menu
#. txt_access_all
msgid "Everything"
msgstr "Alles"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu
msgid "^Try Ubuntu without installing"
msgstr "^Probeer Ubuntu sonder om te installeer"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_kubuntu
msgid "^Try Kubuntu without installing"
msgstr "^Probeer Kubuntu sonder om te installeer"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_edubuntu
msgid "^Try Edubuntu without installing"
msgstr "^Probeer Edubuntu sonder om te installeer"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_xubuntu
msgid "^Try Xubuntu without installing"
msgstr "^Probeer Xubuntu sonder om te installeer"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_mid
msgid "^Try Ubuntu MID without installing"
msgstr "^Probeer Ubuntu MID sonder om te installeer"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_netbook
msgid "^Try Ubuntu Netbook without installing"
msgstr "^Probeer Ubuntu Netbook sonder om installeer"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_kubuntu_netbook
msgid "^Try Kubuntu Netbook without installing"
msgstr "^Probeer Kubuntu Netbook sonder om installeer"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_lubuntu
msgid "^Try Lubuntu without installing"
msgstr "^Probeer Lubuntu sonder om te installeer"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_gnome
msgid "^Try Ubuntu GNOME without installing"
msgstr "^Probeer Ubuntu GNOME sonder installasie"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_mate
msgid "^Try Ubuntu MATE without installing"
msgstr "^Probeer Ubuntu MATE sonder installasie"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_start_kubuntu
msgid "^Start Kubuntu"
msgstr "^Begin Kubuntu"

#. Installation mode.
#. txt_menuitem_driverupdates
msgid "Use driver update disc"
msgstr "Gebruik drywer opdateringsskyf"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_ubuntu
msgid "^Install Ubuntu in text mode"
msgstr "^Installeer Ubuntu in teks-modus"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_kubuntu
msgid "^Install Kubuntu in text mode"
msgstr "^Installeer Kubuntu in teks modus"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_edubuntu
msgid "^Install Edubuntu in text mode"
msgstr "^Installeer Edubuntu in teks modus"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_xubuntu
msgid "^Install Xubuntu in text mode"
msgstr "^Installeer Xubuntu in teks modus"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu
msgid "^Install Ubuntu"
msgstr "^Installeer Ubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_kubuntu
msgid "^Install Kubuntu"
msgstr "^Installeer Kubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_edubuntu
msgid "^Install Edubuntu"
msgstr "^Installeer Edubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_xubuntu
msgid "^Install Xubuntu"
msgstr "^Installeer Xubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_server
msgid "^Install Ubuntu Server"
msgstr "^Installeer Ubuntu Bediener"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_gnome
msgid "^Install Ubuntu GNOME"
msgstr "^Installeer Ubuntu GNOME"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_mate
msgid "^Install Ubuntu MATE"
msgstr "^Installeer Ubuntu MATE"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_enlist_ubuntu_maas
msgid "^Multiple server install with MAAS"
msgstr "^Meervoudige bediener installering met MAAS"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntustudio
msgid "^Install Ubuntu Studio"
msgstr "^Installeer Ubuntu Studio"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_mid
msgid "^Install Ubuntu MID"
msgstr "^Installeer Ubuntu MID"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_netbook
msgid "^Install Ubuntu Netbook"
msgstr "^Installeer Ubuntu Netbook"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_kubuntu_netbook
msgid "^Install Kubuntu Netbook"
msgstr "^Installeer Kubuntu Netbook"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_lubuntu
msgid "^Install Lubuntu"
msgstr "Installeer Lubuntu"

#. Installation mode.
#. txt_menuitem_workstation
msgid "Install a workstation"
msgstr "^Installeer 'n werkstasie"

#. Installation mode.
#. txt_menuitem_server
msgid "Install a server"
msgstr "Installeer 'n bediener"

#. Installation mode.
#. txt_menuitem_oem
msgid "OEM install (for manufacturers)"
msgstr "OEM installering (vir vervaardigers)"

#. Installation mode.
#. txt_menuitem_lamp
msgid "Install a LAMP server"
msgstr "Installeer 'n LAMP bediener"

#. Installation mode.
#. txt_menuitem_ltsp
msgid "Install an LTSP server"
msgstr "Installeer 'n LTSP bediener"

#. Installation mode.
#. txt_menuitem_ltsp_mythbuntu
msgid "Install a Diskless Image Server"
msgstr "Installeer 'n Skyflose Beeld Bediener"

#. Installation mode.
#. txt_menuitem_cli
msgid "Install a command-line system"
msgstr "Installeer 'n bevel-lyn stelsel"

#. Installation mode.
#. txt_menuitem_minimal
msgid "Install a minimal system"
msgstr "Installeer 'n minimale stelsel"

#. Installation mode.
#. txt_menuitem_minimalvm
msgid "Install a minimal virtual machine"
msgstr "Installeer 'n minimale virtuele masjien"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_check
msgid "^Check disc for defects"
msgstr "^Gaan skyf na vir defekte"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_rescue
msgid "^Rescue a broken system"
msgstr "^Redding 'n gebroke stelsel"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_memtest
msgid "Test ^memory"
msgstr "Toets ^geheue"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_hd
msgid "^Boot from first hard disk"
msgstr "^Stewel vanaf eerste harde skyfl"

#. Boot option.
#. txt_option_free
msgid "Free software only"
msgstr "Slegs gratis sagteware"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_dell_factory_recovery
msgid "^Dell Automatic Reinstall"
msgstr "^Dell Outomatiese Herïnstallering"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_mythbuntu
msgid "^Install Mythbuntu"
msgstr "^Installeer Mythbuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_mythbuntu
msgid "^Try Mythbuntu without installing"
msgstr "^Probeer Mythbuntu sonder om te installeer"

#. txt_menuitem_live_ubuntukylin
msgid "^Try Ubuntu Kylin without installing"
msgstr "Probeer Ubuntu Kylin sonder om te installeer"

#. txt_menuitem_live_ubuntukylin_netbook
msgid "^Try Ubuntu Kylin Netbook without installing"
msgstr "Probeer Ubuntu Kylin Netboek sonder om te installeer"

#. txt_menuitem_install_text_ubuntukylin
msgid "^Install Ubuntu Kylin in text mode"
msgstr "Installeer Ubuntu Kylin in teks mode"

#. txt_menuitem_install_ubuntukylin
msgid "^Install Ubuntu Kylin"
msgstr "Installeer Ubuntu Kylin"

#. txt_menuitem_install_ubuntukylin_netbook
msgid "^Install Ubuntu Kylin Netbook"
msgstr "Installeer Ubuntu Kylin Netboek"

#. txt_menuitem_start_ubuntukylin
msgid "^Start Ubuntu Kylin"
msgstr "^Begin Ubuntu Kylin"

#. txt_menuitem_live_ubuntustudio
msgid "^Try Ubuntu Studio without installing"
msgstr "^Probeer Ubuntu Studio sonder om te installeer"

#. txt_install_ubuntu_safegraphics
msgid "^Install Ubuntu (safe graphics)"
msgstr "^ Installeer Ubuntu (veilig grafika)"

#. txt_install_edubuntu_safegraphics
msgid "^Install Edubuntu (safe graphics)"
msgstr "^ Installeer Edubuntu (veilig grafika)"

#. txt_install_kubuntu_safegraphics
msgid "^Install Kubuntu (safe graphics)"
msgstr "^ Installeer Kubuntu (veilig grafika)"

#. txt_install_xubuntu_safegraphics
msgid "^Install Xubuntu (safe graphics)"
msgstr "^ Installeer Xubuntu (veilig grafika)"

#. txt_install_ubuntu_mid_safegraphics
msgid "^Install Ubuntu MID (safe graphics)"
msgstr "^ Installeer Ubuntu MID (veilig grafika)"

#. txt_install_ubuntu_netbook_safegraphics
msgid "^Install Ubuntu Netbook (safe graphics)"
msgstr "^ Installeer Ubuntu Netbook (veilig grafika)"

#. txt_install_kubuntu_netbook_safegraphics
msgid "^Install Kubuntu Netbook (safe graphics)"
msgstr "^ Installeer Kubuntu Netbook (veilig grafika)"

#. txt_install_lubuntu_safegraphics
msgid "^Install Lubuntu (safe graphics)"
msgstr "^ Installeer Lubuntu (veilig grafika)"

#. txt_install_ubuntu_gnome_safegraphics
msgid "^Install Ubuntu GNOME (safe graphics)"
msgstr "^ Installeer Ubuntu GNOME (veilig grafika)"

#. txt_install_ubuntu_mate_safegraphics
msgid "^Install Ubuntu MATE (safe graphics)"
msgstr "^ Installeer Ubuntu MATE (veilig grafika)"

#. txt_install_mythbuntu_safegraphics
msgid "^Install Mythbuntu (safe graphics)"
msgstr "^ Installeer Mythbuntu (veilig grafika)"

#. txt_install_ubuntu_kylin_safegraphics
msgid "^Install Ubuntu Kylin (safe graphics)"
msgstr "^ Installeer Ubuntu Kylin (veilig grafika)"

#. txt_install_ubuntu_kylin_netbook_safegraphics
msgid "^Install Ubuntu Kylin Netbook (safe graphics)"
msgstr "^ Installeer Ubuntu Kylin Netbook (veilig grafika)"

#. txt_install_ubuntu_studio_safegraphics
msgid "^Install Ubuntu Studio (safe graphics)"
msgstr "^ Installeer Ubuntu Studio (veilig grafika)"

#. txt_try_ubuntu_safegraphics
msgid "^Try Ubuntu without installing (safe graphics)"
msgstr "^ Probeer Ubuntu sonder om te installeer (veilig grafika)"

#. txt_try_edubuntu_safegraphics
msgid "^Try Edubuntu without installing (safe graphics)"
msgstr "^ Probeer Edubuntu sonder om te installeer (veilig grafika)"

#. txt_try_kubuntu_safegraphics
msgid "^Try Kubuntu without installing (safe graphics)"
msgstr "^ Probeer Kubuntu sonder om te installeer (veilig grafika)"

#. txt_try_xubuntu_safegraphics
msgid "^Try Xubuntu without installing (safe graphics)"
msgstr "^ Probeer Xubuntu sonder om te installeer (veilig grafika)"

#. txt_try_ubuntu_mid_safegraphics
msgid "^Try Ubuntu MID without installing (safe graphics)"
msgstr "^ Probeer Ubuntu MID sonder om te installeer (veilig grafika)"

#. txt_try_ubuntu_netbook_safegraphics
msgid "^Try Ubuntu Netbook without installing (safe graphics)"
msgstr "^ Probeer Ubuntu Netbook sonder om te installeer (veilig grafika)"

#. txt_try_kubuntu_netbook_safegraphics
msgid "^Try Kubuntu Netbook without installing (safe graphics)"
msgstr "^ Probeer Kubuntu Netbook sonder om te installeer (veilig grafika)"

#. txt_try_lubuntu_safegraphics
msgid "^Try Lubuntu without installing (safe graphics)"
msgstr "^ Probeer Lubuntu sonder om te installeer (veilig grafika)"

#. txt_try_ubuntu_gnome_safegraphics
msgid "^Try Ubuntu GNOME without installing (safe graphics)"
msgstr "^ Probeer Ubuntu GNOME sonder om te installeer (veilig grafika)"

#. txt_try_ubuntu_mate_safegraphics
msgid "^Try Ubuntu MATE without installing (safe graphics)"
msgstr "^ Probeer Ubuntu MATE sonder om te installeer (veilig grafika)"

#. txt_try_mythbuntu_safegraphics
msgid "^Try Mythbuntu without installing (safe graphics)"
msgstr "^ Probeer Mythbuntu sonder om te installeer (veilig grafika)"

#. txt_try_ubuntu_kylin_safegraphics
msgid "^Try Ubuntu Kylin without installing (safe graphics)"
msgstr "^ Probeer Ubuntu Kylin sonder om te installeer (veilig grafika)"

#. txt_try_ubuntu_kylin_netbook_safegraphics
msgid "^Try Ubuntu Kylin Netbook without installing (safe graphics)"
msgstr ""
"^ Probeer Ubuntu Kylin Netbook sonder om te installeer (veilig grafika)"

#. txt_try_ubuntu_studio_safegraphics
msgid "^Try Ubuntu Studio without installing (safe graphics)"
msgstr "^ Probeer Ubuntu Studio sonder om te installeer (veilig grafika)"
