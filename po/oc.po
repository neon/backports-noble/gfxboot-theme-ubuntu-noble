# Occitan (post 1500) translation for gfxboot-theme-ubuntu
# Copyright (c) (c) 2006 Canonical Ltd, and Rosetta Contributors 2006
# This file is distributed under the same license as the gfxboot-theme-ubuntu package.
# Yannig MARCHEGAY (Kokoyaya)  <yannig@marchegay.org>, 2006.
# , fuzzy
#
#
msgid ""
msgstr ""
"Project-Id-Version: gfxboot-theme-ubuntu\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2016-04-16 01:46+0000\n"
"PO-Revision-Date: 2016-10-17 07:44+0000\n"
"Last-Translator: Cédric VALMARY (Tot en òc) <cvalmary@yahoo.fr>\n"
"Language-Team: Occitan (post 1500) <oc@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2020-04-10 15:08+0000\n"
"X-Generator: Launchpad (build 2e26c9bbd21cdca248baaea29aeffb920afcc32a)\n"

#. ok button label
#. txt_ok
msgid "OK"
msgstr "D'acòrdi"

#. cancel button label
#. txt_cancel
msgid "Cancel"
msgstr "Anullar"

#. reboot button label
#. txt_reboot
msgid "Reboot"
msgstr "Reaviar"

#. continue button label
#. txt_continue
msgid "Continue"
msgstr "Contunhar"

#. txt_bootoptions
msgid "Boot Options"
msgstr "Opcions d'aviada"

#. window title for exit dialog
#. txt_exit_title (see txt_exit_dialog)
msgid "Exiting..."
msgstr "Tampadura..."

#. txt_exit_dialog
msgid ""
"You are leaving the graphical boot menu and\n"
"starting the text mode interface."
msgstr ""
"Sètz a sortir del menú d'aviada grafica e\n"
"dintratz dins l'interfàcia en mòde tèxte."

#. txt_help
msgid "Help"
msgstr "Ajuda"

#. info box title
#. txt_info_title
msgid "Boot loader"
msgstr "Gestionari d'aviada"

#. error box title
#. txt_error_title
msgid "I/O error"
msgstr "Error d'E/S"

#. boot disk change dialog title
#. txt_change_disk_title
msgid "Change Boot Disk"
msgstr "Cambiar la disc d'aviada"

#. txt_insert_disk
#, c-format
msgid "Insert boot disk %u."
msgstr "Inserir lo disc d'aviada %u."

#. txt_insert_disk2
#, c-format
msgid ""
"This is boot disk %u.\n"
"Insert boot disk %u."
msgstr ""
"Aquò's lo disc d'aviada %u.\n"
"Inserissètz lo disc d'aviada %u."

#. txt_insert_disk3
#, c-format
msgid ""
"This is not a suitable boot disk.\n"
"Please insert boot disk %u."
msgstr ""
"Aquò es pas un disc d'aviada adaptat.\n"
"Inserissètz lo disc d'aviada %u."

#. password dialog title
#. txt_password_title
msgid "Password"
msgstr "Senhal"

#. Keep the newlines and spaces after ':'!
#. txt_password
msgid ""
"Enter your password:   \n"
"\n"
"\n"
msgstr ""
"Picatz vòstre senhal :   \n"
"\n"
"\n"

#. dvd warning title
#. txt_dvd_warning_title
msgid "DVD Error"
msgstr "Error de DVD"

#. txt_dvd_warning2
msgid ""
"This is a two-sided DVD. You have booted from the second side.\n"
"\n"
"Turn the DVD over then continue."
msgstr ""
"Aquò's es un DVD dobla fàcia. Avètz aviat a partir de la segonda fàcia.\n"
"\n"
"Reviratz lo DVD puèi contunhatz."

#. power off dialog title
#. txt_power_off_title
msgid "Power Off"
msgstr "Atudar"

#. txt_power_off
msgid "Halt the system now?"
msgstr "Arrestar lo sistèma ara ?"

#. txt_password
msgid "Password\n"
msgstr "Senhal\n"

#. button label for other/more options
#. txt_other_options
msgid "Other Options"
msgstr "Autras opcions"

#. label for language selection
#. txt_language
msgid "Language"
msgstr "Lenga"

#. label for keymap selection
#. txt_keymap
msgid "Keymap"
msgstr "Agençament del clavièr"

#. label for installation mode selection
#. txt_modes
msgid "Modes"
msgstr "Mòdes"

#. label for modes menu
#. txt_mode_normal
msgid "Normal"
msgstr "Normal"

#. label for d-i mode menu
#. txt_expert_mode
msgid "Expert mode"
msgstr "Mòde expèrt"

#. title for accessibility menu
#. txt_access
msgid "Accessibility"
msgstr "Accessibilitat"

#. label for accessibility menu
#. txt_access_none
msgid "None"
msgstr "Pas cap"

#. label for accessibility menu
#. txt_access_v1
msgid "High Contrast"
msgstr "Contraste elevat"

#. label for accessibility menu
#. txt_access_v2
msgid "Magnifier"
msgstr "Lópia"

#. label for accessibility menu
#. txt_access_v3
msgid "Screen Reader"
msgstr "Lector d'ecran"

#. label for accessibility menu
#. txt_access_brltty
msgid "Braille Terminal"
msgstr "Terminal Braille"

#. label for accessibility menu
#. txt_access_m1
msgid "Keyboard Modifiers"
msgstr "Modificadors de clavièr"

#. label for accessibility menu
#. txt_access_m2
msgid "On-Screen Keyboard"
msgstr "Clavièr a l'ecran"

#. label for accessibility menu
#. txt_access_m3
msgid "Motor Difficulties - switch devices"
msgstr "Dificultats Motrises - interruptors"

#. label for accessibility menu
#. txt_access_all
msgid "Everything"
msgstr "Tot"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu
msgid "^Try Ubuntu without installing"
msgstr "^Ensajar Ubuntu sens l'installar"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_kubuntu
msgid "^Try Kubuntu without installing"
msgstr "^Ensajar Kubuntu sens l'installar"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_edubuntu
msgid "^Try Edubuntu without installing"
msgstr "^Ensajar Edubuntu sens l'installar"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_xubuntu
msgid "^Try Xubuntu without installing"
msgstr "^Ensajar Xubuntu sens l'installar"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_mid
msgid "^Try Ubuntu MID without installing"
msgstr "^Ensajar Ubuntu MID sens l'installar"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_netbook
msgid "^Try Ubuntu Netbook without installing"
msgstr "^Ensajar Ubuntu Netbook sens l'installar"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_kubuntu_netbook
msgid "^Try Kubuntu Netbook without installing"
msgstr "^Ensajar Kubuntu Netbook sens l'installar"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_lubuntu
msgid "^Try Lubuntu without installing"
msgstr "^Ensajar Lubuntu sens l'installar"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_gnome
msgid "^Try Ubuntu GNOME without installing"
msgstr "^Ensajar Ubuntu GNOME sens l'installar"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_mate
msgid "^Try Ubuntu MATE without installing"
msgstr "^Ensajar Ubuntu MATE sens l'installar"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_start_kubuntu
msgid "^Start Kubuntu"
msgstr "^Aviar Kubuntu"

#. Installation mode.
#. txt_menuitem_driverupdates
msgid "Use driver update disc"
msgstr "Utilizar lo disc de mesa a jorn de pilòts"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_ubuntu
msgid "^Install Ubuntu in text mode"
msgstr "^Installar Ubuntu en mòde tèxte"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_kubuntu
msgid "^Install Kubuntu in text mode"
msgstr "^Installar Kubuntu en mòde tèxte"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_edubuntu
msgid "^Install Edubuntu in text mode"
msgstr "^Installar Edubuntu en mòde tèxte"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_xubuntu
msgid "^Install Xubuntu in text mode"
msgstr "^Installar Xubuntu en mòde tèxte"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu
msgid "^Install Ubuntu"
msgstr "^Installar Ubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_kubuntu
msgid "^Install Kubuntu"
msgstr "^Installar Kubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_edubuntu
msgid "^Install Edubuntu"
msgstr "^Installar Edubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_xubuntu
msgid "^Install Xubuntu"
msgstr "^Installar Xubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_server
msgid "^Install Ubuntu Server"
msgstr "^Installar Ubuntu Server"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_gnome
msgid "^Install Ubuntu GNOME"
msgstr "^Installar Ubuntu GNOME"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_mate
msgid "^Install Ubuntu MATE"
msgstr "^Installar Ubuntu MATE"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_enlist_ubuntu_maas
msgid "^Multiple server install with MAAS"
msgstr "^Installacion d'un servidor multiple amb MAAS"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntustudio
msgid "^Install Ubuntu Studio"
msgstr "^Installar Ubuntu Studio"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_mid
msgid "^Install Ubuntu MID"
msgstr "^Installar Ubuntu MID"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_netbook
msgid "^Install Ubuntu Netbook"
msgstr "^Installar Ubuntu Netbook"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_kubuntu_netbook
msgid "^Install Kubuntu Netbook"
msgstr "^Installar Kubuntu Netbook"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_lubuntu
msgid "^Install Lubuntu"
msgstr "^Installar Lubuntu"

#. Installation mode.
#. txt_menuitem_workstation
msgid "Install a workstation"
msgstr "Installar un pòste de trabalh"

#. Installation mode.
#. txt_menuitem_server
msgid "Install a server"
msgstr "Installar un servidor"

#. Installation mode.
#. txt_menuitem_oem
msgid "OEM install (for manufacturers)"
msgstr "Installacion OEM (pels integradors)"

#. Installation mode.
#. txt_menuitem_lamp
msgid "Install a LAMP server"
msgstr "Installar un servidor LAMP"

#. Installation mode.
#. txt_menuitem_ltsp
msgid "Install an LTSP server"
msgstr "Installar un servidor LTSP"

#. Installation mode.
#. txt_menuitem_ltsp_mythbuntu
msgid "Install a Diskless Image Server"
msgstr "Installar un imatge de servidor sens disc"

#. Installation mode.
#. txt_menuitem_cli
msgid "Install a command-line system"
msgstr "Installar un sistèma en linha de comanda"

#. Installation mode.
#. txt_menuitem_minimal
msgid "Install a minimal system"
msgstr "Installar un sistèma minimal"

#. Installation mode.
#. txt_menuitem_minimalvm
msgid "Install a minimal virtual machine"
msgstr "Installar una maquina virtuala minimala"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_check
msgid "^Check disc for defects"
msgstr "^Verificar se lo disc a de defauts"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_rescue
msgid "^Rescue a broken system"
msgstr "^Reparar un sistèma damatjat"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_memtest
msgid "Test ^memory"
msgstr "Testar la ^memòria"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_hd
msgid "^Boot from first hard disk"
msgstr "^Aviar a partir del primièr disc dur"

#. Boot option.
#. txt_option_free
msgid "Free software only"
msgstr "Logicials liures solament"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_dell_factory_recovery
msgid "^Dell Automatic Reinstall"
msgstr "^Reïnstallacion automatica Dell"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_mythbuntu
msgid "^Install Mythbuntu"
msgstr "^Installar Mythbuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_mythbuntu
msgid "^Try Mythbuntu without installing"
msgstr "^Ensajar Mythbuntu sens l'installar"

#. txt_menuitem_live_ubuntukylin
msgid "^Try Ubuntu Kylin without installing"
msgstr "^Ensajar Ubuntu Kylin sens installacion"

#. txt_menuitem_live_ubuntukylin_netbook
msgid "^Try Ubuntu Kylin Netbook without installing"
msgstr "^Ensajar Ubuntu Kylin Netbook sens installacion"

#. txt_menuitem_install_text_ubuntukylin
msgid "^Install Ubuntu Kylin in text mode"
msgstr "^Installar Ubuntu Kylin en mòde tèxte"

#. txt_menuitem_install_ubuntukylin
msgid "^Install Ubuntu Kylin"
msgstr "^Installar Ubuntu Kylin"

#. txt_menuitem_install_ubuntukylin_netbook
msgid "^Install Ubuntu Kylin Netbook"
msgstr "^Installar Ubuntu Kylin Netbook"

#. txt_menuitem_start_ubuntukylin
msgid "^Start Ubuntu Kylin"
msgstr "^Aviar Ubuntu Kylin"

#. txt_menuitem_live_ubuntustudio
msgid "^Try Ubuntu Studio without installing"
msgstr "^Ensajar Ubuntu Studio sens l'installar"

#. txt_install_ubuntu_safegraphics
msgid "^Install Ubuntu (safe graphics)"
msgstr ""

#. txt_install_edubuntu_safegraphics
msgid "^Install Edubuntu (safe graphics)"
msgstr ""

#. txt_install_kubuntu_safegraphics
msgid "^Install Kubuntu (safe graphics)"
msgstr ""

#. txt_install_xubuntu_safegraphics
msgid "^Install Xubuntu (safe graphics)"
msgstr ""

#. txt_install_ubuntu_mid_safegraphics
msgid "^Install Ubuntu MID (safe graphics)"
msgstr ""

#. txt_install_ubuntu_netbook_safegraphics
msgid "^Install Ubuntu Netbook (safe graphics)"
msgstr ""

#. txt_install_kubuntu_netbook_safegraphics
msgid "^Install Kubuntu Netbook (safe graphics)"
msgstr ""

#. txt_install_lubuntu_safegraphics
msgid "^Install Lubuntu (safe graphics)"
msgstr ""

#. txt_install_ubuntu_gnome_safegraphics
msgid "^Install Ubuntu GNOME (safe graphics)"
msgstr ""

#. txt_install_ubuntu_mate_safegraphics
msgid "^Install Ubuntu MATE (safe graphics)"
msgstr ""

#. txt_install_mythbuntu_safegraphics
msgid "^Install Mythbuntu (safe graphics)"
msgstr ""

#. txt_install_ubuntu_kylin_safegraphics
msgid "^Install Ubuntu Kylin (safe graphics)"
msgstr ""

#. txt_install_ubuntu_kylin_netbook_safegraphics
msgid "^Install Ubuntu Kylin Netbook (safe graphics)"
msgstr ""

#. txt_install_ubuntu_studio_safegraphics
msgid "^Install Ubuntu Studio (safe graphics)"
msgstr ""

#. txt_try_ubuntu_safegraphics
msgid "^Try Ubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_edubuntu_safegraphics
msgid "^Try Edubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_kubuntu_safegraphics
msgid "^Try Kubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_xubuntu_safegraphics
msgid "^Try Xubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_mid_safegraphics
msgid "^Try Ubuntu MID without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_netbook_safegraphics
msgid "^Try Ubuntu Netbook without installing (safe graphics)"
msgstr ""

#. txt_try_kubuntu_netbook_safegraphics
msgid "^Try Kubuntu Netbook without installing (safe graphics)"
msgstr ""

#. txt_try_lubuntu_safegraphics
msgid "^Try Lubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_gnome_safegraphics
msgid "^Try Ubuntu GNOME without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_mate_safegraphics
msgid "^Try Ubuntu MATE without installing (safe graphics)"
msgstr ""

#. txt_try_mythbuntu_safegraphics
msgid "^Try Mythbuntu without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_kylin_safegraphics
msgid "^Try Ubuntu Kylin without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_kylin_netbook_safegraphics
msgid "^Try Ubuntu Kylin Netbook without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_studio_safegraphics
msgid "^Try Ubuntu Studio without installing (safe graphics)"
msgstr ""
